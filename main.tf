resource "azurerm_management_group" "child_group" {
  display_name               = var.child_group_name
  #subscription_ids = var.subscription_ids 
  subscription_ids = [var.subscription_ids]
}


data "azurerm_policy_set_definition" "reference" {
  display_name = var.policy_name
}

resource "azurerm_management_group_policy_assignment" "policy_assignment" {
  depends_on = [azurerm_management_group.child_group]
  name                 = "policy-association"
  policy_definition_id = data.azurerm_policy_set_definition.reference.id
  management_group_id  = azurerm_management_group.child_group.id
  location = var.location
  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_resource_group" "rg" {
  name     = var.rg_name
  location = var.location

  tags = {
    environment = var.environment
  }
}

resource "azurerm_network_security_group" "sg" {
  name                = var.security_group_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "Terraform"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

/*resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = var.vnet_cidr 
  #address_space       = [var.vnet_cidr] 
  

  tags = {
    environment = var.environment
  }
}

resource "azurerm_subnet" "subnet" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = var.subnet_cidr
  #address_prefixes     = [var.subnet_cidr]
}*/
