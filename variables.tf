#variable "billing_account_name" {
#  type    = string
#  default = ""
#  description = "Please provide billing account name in-order to create subscription"
#}
#
#variable "enrollment_account_name" {
#  type    = string
#  default = ""
#  description = "Please provide enrollment account name in-order to create subscription"
#}
#
#variable "subscription_name" {
#  type    = string
#  default = ""
#  description = "Please provide subscription name to create new one"
#}
#variable "policy_assignment_name" {
#  type    = string
#  default = "user_azure_policy"
#}
#
#variable "password" {
###
#variable "mail_nickname" {
#  type    = list
#  default = ["Srini",]
#}
#variable "user_display_name" {
#  type    = list
# default = ["Srini",]
#}
#variable "user_principal_name" {
#  default = ["srini@gdit.com",]
#}


variable "parent_group_id" {
  type    = string
  default = ""
  
}
variable "ARM_TENANT_ID" {
  #type    = list(string)
  type    =  string
  default = "6d6689c4-179e-41f7-9038-f2eaa4ca25f3"
  description = "Please provide Tenant id id's to attach to management group"
}
variable "subscription_ids" {
  #type    = list(string)
  type    =  string
  default = "484de8b7-695b-4073-8058-dff14463e521"
  description = "Please provide subscription id's to attach to management group"
}
variable "subscription_id" {
  type    = string
  default = "33fb4946-cde5-4ea0-a183-c737f9999165"
  description = "Please provide subscription id's to attach to management group"
}
variable "ARM_CLIENT_ID" {
  type    = string
  default = "67aae1e0-18d3-4f6a-b277-71b72be51409"
 description = "Please provide subscription id's to attach to management group"
}
variable "ARM_CLIENT_SECRET" {
   type    = string
  default = "xZ.7Q~CHOfIe0G1AsV57vqbsi5.LYqERLmwgO"
 description = "Please provide subscription id's to attach to management group"
}

#variable "workload" {
#  type    = string
#  default = ""
#  description = "Please provide workload as Dev, Test etc"
#}
#
#variable "parent_group_name" {
#  type    = string
#  default = ""
#  description = "Please provide name for parent management group"
#}

variable "child_group_name" {
  type    = string
  default = ""
  description = "Please provide name for child management group"
}

variable "policy_name" {
  type    = string
  default = ""
  description = "Please provide policy name to attach it to parent management group"
}

variable "rg_name" {
  type    = string
  default = ""
  description = "Please provide name for resource group"
}
variable "environment" {
  type    = string
  default = ""
 }

variable "location" {
  type    = string
  default = ""
  description = "Please provide location in which you want to create resource group"
}

variable "security_group_name" {
  type    = string
  default = ""
}

/*variable "vnet_name" {
  type    = string
  default = ""
}
variable "subnet_name" {
  type    = string
  default = ""
}
variable "vnet_cidr" {
 type    = list(string)
 #type    =  string
  default = ["10.0.0.0/16"]
}

variable "subnet_cidr" {
  type    = list(string)
  #type    =  string
   default = ["10.0.0.0/25"]
}*/
