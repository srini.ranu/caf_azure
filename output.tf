#output "parent group name" {
#    value = azurerm_management_group.parent_group.name
#}
#
#output "parent group id" {
#    value = azurerm_management_group.parent_group.id
#}

output "child_group_name" {
    value = azurerm_management_group.child_group.name
}

#output "child group id" {
#    value = azurerm_management_group.child_group.id
#}
#
#output "subscription name" {
#    value = azurerm_subscription.subscription.name
#}

#output "subscription id" {
#    value = azurerm_subscription.subscription.id
#}

output "resource_group_name" {
    value = azurerm_resource_group.rg.name
}
